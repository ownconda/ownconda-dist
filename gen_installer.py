#!/bin/env python3
"""
Generate a constructor file for Python X.Y and build the installer from it.

Usage:
  python gen_installer.py DEST_DIR X.Y [dev]

"""
import datetime
import pathlib
import subprocess
import sys


# Aguments
DEST = sys.argv[1]
PYVER = sys.argv[2]
ISDEV = (len(sys.argv) == 4 and sys.argv[3] == 'dev')

# Config
NAME = "ownconda"
VERSION = f'{datetime.datetime.today():%Y.%m.%d}'
INSTALLER = f'ownconda{"-dev" if ISDEV else ""}-{PYVER}'
PYTHON = f'python {PYVER}*'
POST_INSTALL = '../post_install.sh'
CHANNELS = [
    'https://forge.services.own/conda/stable',
]
SPECS = [
    PYTHON,
    'conda',
    'pip',
]
if ISDEV:
    SPECS += [
        'conda-build',
        'own-conda-tools',
    ]

# Create constructor
SPECS = '\n'.join(f'  - {spec}' for spec in SPECS)
CHANNELS = '\n'.join(f'  - {channel}' for channel in CHANNELS)
CONSTRUCT_YAML = f"""
name: {NAME}
version: {VERSION}
installer_filename: {INSTALLER}.sh

channels:
{CHANNELS}

specs:
{SPECS}

post_install: {POST_INSTALL}
""".lstrip()


INSTALLER = pathlib.Path(INSTALLER)
CONSTRUCT = INSTALLER / 'construct.yaml'
print(f'# {CONSTRUCT}')
print(CONSTRUCT_YAML)

INSTALLER.mkdir(exist_ok=True)
CONSTRUCT.write_text(CONSTRUCT_YAML)
subprocess.run(['constructor', str(INSTALLER)])
subprocess.run(['mv', f'{INSTALLER}.sh', DEST])
