#! /bin/bash
DEFAULT_REPO='https://forge.services.own/conda'
DEFAULT_CHANNEL="${DEFAULT_REPO}/stable/"

if [[ -z "$SKIP_OWNCONDA_UPDATE" ]]; then
    echo ''
    echo -ne "\e[00;34m"
    echo 'Updating installation ...'
    echo -n 'You can skip this step by exporting '
    echo -ne "\e[01;34m"
    echo -n 'SKIP_OWNCONDA_UPDATE=1'
    echo -e "\e[00m"
    $PREFIX/bin/conda update --quiet --name=root --all --yes
fi

#
# Create .condarc in the root environment
#
cat <<EOF >$PREFIX/.condarc
# Channels to search for conda packages.
channels:
  # unfortunately the defaults-channel does not work with Conda 4.2 right now
  # - defaults
  - $DEFAULT_CHANNEL


default_channels:
  - $DEFAULT_CHANNEL

# Setting a channel alias will allow you to to run, e.g.,
# "conda install --channel dev <package>" instead of
# "install --channel ${DEFAULT_REPO}/staging <package>".
channel_alias: $DEFAULT_REPO

# Choose the "yes" option whenever asked? (same as "--yes" argument)
#always_yes: False

# Show channel URLs when displaying what is going to be downloaded and in
# "conda list".
#show_channel_urls: False

# Add pip, wheel and setuptools as dependencies of python.  This ensures pip,
# wheel and setuptools will always be installed any time python is installed.
#add_pip_as_python_dependency: True

# Use pip when listing packages with "conda list".  Note that this does not
# affect any conda command or functionality other than the output of the
# command "conda list".
#use_pip: True

# Filters out all channels URLs which do not use the "file://" protocol.
#offline: False

# When True, conda updates itself any time a user updates or installs a package
# in the root environment. When False, conda updates itself only if the user
# manually issues a conda update command.
#auto_update_conda: True

# By default, conda install updates the given package and all its dependencies
# to the latest versions.
#update_dependencies: True

# Automatically upload packages built with conda build to Anaconda.org.
anaconda_upload: False

# When creating new environments add these packages by default.  You can
# override this option at the command prompt with the "--no-default-packages"
# flag.  The default is not to include any packages.
#create_default_packages: []

# Enable certain features to be tracked by default.  The default is to not
# track any features.  This is similar to adding "mkl" to the
# "create_default_packages" list.
#track_features: []

# You can move the build directory to another partition or you can share
# the build directory between multiple ownconda installations
#conda-build:
#  root-dir: $PREFIX/conda-bld

# Chosse your datacenter's http-proxy if required
#proxy_servers:
#  http: http://proxy.own:8080

# Don't add hashes to the build string
conda-build:
  filename_hashing: false

EOF

echo ''
echo -ne "\033[01;32m"
echo 'INFO:'
echo -ne "\033[00;32m"
echo "You can edit \"$PREFIX/.condarc\" to adjust"
echo 'the settings for this Conda installation.'
echo -e "\033[00;00m"

#
# Check if the user has a ~/.condarc file that needs to be relocated or fixed:
#
if [[ -f $HOME/.condarc ]]; then
    echo -ne "\033[01;33m"
    echo 'NOTE:'
    echo -ne "\033[00;33m"
    echo 'Detected a ".condarc" in your Home directory.  This can lead to'
    echo 'problown if you have multiple Conda distributions installed.'
    echo 'Please consider moving the .condarc into the root Conda'
    echo 'environment, e.g., to "~/ownconda/".'
    echo -e "\033[00;00m"

    if [[ ! $(grep "  \- $DEFAULT_CHANNEL" $HOME/.condarc) ]]; then
        echo -ne "\033[01;31m"
        echo 'WARNING:'
        echo -ne "\033[00;31m"
        echo 'If you don’t want to move your "~/.condarc", add the channel'
        echo "\"$DEFAULT_CHANNEL\" to it!"
        echo -e "\033[00;00m"
    fi
fi

echo -ne "\033[01;33m"
echo 'WARNING:'
echo -ne "\033[00;33m"
echo 'The installer will ask you if you want to initialize ownconda.'
echo 'Please answer with NO, because that feature is still buggy.'
echo -ne "\033[00;32m"
echo 'Instead you can add the following lines to your .bashrc:'
echo -e "\033[01;37m"
echo "    source \"$PREFIX/etc/profile.d/conda.sh\""
echo "    if [[ \":\$PATH:\" != *\":$PREFIX/bin:\"* ]]; then"
echo "        export PATH=\"$PREFIX/bin:\$PATH\""
echo "    fi"
echo "    source <($PREFIX/bin/ownconda completion)"
echo -e "\033[00;00m"
