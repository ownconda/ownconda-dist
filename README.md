*ownconda* Conda distribution
=============================

This repository contains the
[constructor](https://github.com/conda/constructor) file for creating our
own Conda distribution (named *ownconda*).

## Usage

Install the `constructor` package into a (new) Conda env:

```bash
$ conda create -n ownconda constructor
```

Run the generator script and pass the Python version to use.  If you want a
development installer (that includes conda-build and the ownconda tools), also
pass `dev`:

```bash
$ ./gen_installer.py ./ 3.7  # Minimal installer
$ ./gen_installer.py ./ 3.7 dev  # Development installer
```

The file `./ownconda[-dev]-3.7.sh` now contains the self-extracting installer
for our distribution.
